module RIO.Local
  ( module RIO
  , module RIO.Local
  , module RE
  ) where

import RIO

import GHC.Float as RE (double2Float, float2Double)
import RIO.State as RE (gets)

τ :: Float
τ = 2 * pi
