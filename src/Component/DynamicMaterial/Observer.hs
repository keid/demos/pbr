module Component.DynamicMaterial.Observer
  ( Buffer
  , Observer
  , newObserver
  , observe
  ) where

import RIO.Local

import Control.Monad.Trans.Resource (ResourceT)
import Data.Type.Equality (type (~))
import Engine.Types qualified as Engine
import Engine.Vulkan.Types (MonadVulkan)
import Engine.Worker qualified as Worker
import Render.Lit.Material (Material)
import Resource.Buffer qualified as Buffer
import Vulkan.Core10 qualified as Vk

import Component.DynamicMaterial.Process qualified as Process

type Buffer = Buffer.Allocated 'Buffer.Coherent Material
type Observer = Worker.ObserverIO Buffer

newObserver :: Int -> ResourceT (Engine.StageRIO st) Observer
newObserver numMaterials =
  Buffer.newObserverCoherent
    "DynamicMaterial"
    Vk.BUFFER_USAGE_UNIFORM_BUFFER_BIT
    numMaterials
    mempty

observe
  :: ( MonadVulkan env m
     , Worker.HasOutput source
     , Worker.GetOutput source ~ Process.Output
     )
  => source
  -> Observer
  -> m ()
observe = Buffer.observeCoherentResize_
