module Component.DynamicMaterial.Process
  ( Input(..)
  , Component.DynamicMaterial.Process.Cell
  , Component.DynamicMaterial.Process.Merge
  , spawn
  , Output
  ) where

import RIO.Local

import Engine.Worker qualified as Worker
import Geomancy (Vec3, vec2)
import Geomancy.Vec4 qualified as Vec4
import Render.Lit.Material (Material)
import Render.Lit.Material qualified as Material
import RIO.Vector.Storable qualified as Storable
import UnliftIO.Resource (MonadResource)
import Vulkan.Zero (zero)

data Input = Input
  { metal     :: Bool
  , roughness :: Float
  , baseColor :: Vec3
  }
type Cell = Worker.Cell Input Output
type Merge = Worker.Merge Output
type Output = Storable.Vector Material

spawn
  :: ( MonadResource m
     , MonadUnliftIO m
     )
  => Vector Input
  -> m (Vector Cell, Merge)
spawn inputs = do
  cells <- traverse (Worker.spawnCell mkMaterial) inputs
  merged <- Worker.spawnMergeT (Storable.concat . toList) cells
  pure (cells, merged)

mkMaterial :: Input -> Output
mkMaterial Input{..} =
  Storable.singleton $ zero
    { Material.mBaseColor =
        Vec4.fromVec3 baseColor 1
    , Material.mMetallicRoughness =
        vec2 (bool 0 1 metal) roughness
    }
