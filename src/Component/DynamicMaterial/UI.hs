module Component.DynamicMaterial.UI
  ( ui
  ) where

import RIO

import Data.Type.Equality (type (~))
import DearImGui qualified
import Engine.Worker qualified as Worker
import Geomancy (vec3, withVec3)

import Component.DynamicMaterial.Process qualified as Process

ui
  :: ( Worker.HasInput process
     , Worker.GetInput process ~ Process.Input
     , MonadIO m
     )
  => process
  -> m ()
ui process = do
  void $!
    DearImGui.checkbox "Metal" $
      Worker.stateVarMap
        Process.metal
        ( \new input -> input
            { Process.metal = new
            }
        )
        process

  void $!
    DearImGui.sliderFloat
      "Roughness"
      (Worker.stateVarMap
        Process.roughness
        ( \new input -> input
            { Process.roughness = new
            }
        )
        process
      )
      0.0
      1.0

  void $!
    DearImGui.colorPicker3 "Base Color" $
      Worker.stateVarMap
        (flip withVec3 DearImGui.ImVec3 . Process.baseColor)
        ( \(DearImGui.ImVec3 r g b) input -> input
              { Process.baseColor = vec3 r g b
              }
        )
        process
