module Component.DynamicMaterial.Models
  ( materialSphere
  , StagedModel
  ) where

import RIO

import Geomancy (Vec3)
import Geomancy.Vec3 qualified as Vec3
import Geometry.Icosphere qualified as Icosphere
import Render.Lit.Material.Model qualified as LitMaterial
import Resource.Buffer qualified as Buffer
import Engine.Vulkan.Types (MonadVulkan, Queues)
import Resource.Model qualified as Model
import RIO.Vector.Storable qualified as Storable
import Vulkan.Core10 qualified as Vk

type StagedModel = LitMaterial.Model 'Buffer.Staged

materialSphere
  :: MonadVulkan env m
  => Queues Vk.CommandPool
  -> Word32
  -> m StagedModel
materialSphere pools materialIx =
  Model.createStaged
    (Just $ "materialSphere " <> fromString (show materialIx))
    pools
    positions
    (Storable.map mkAttrs positions)
    indices
  where
    (positions, indices) = generate_ 4

    mkAttrs pos = LitMaterial.VertexAttrs
      { vaNormal    = pos
      , vaMaterial  = materialIx
      , vaTexCoord0 = 0
      , vaTexCoord1 = 0
      , vaTangent   = 0
      }

-- TODO: extract to geometry
generate_
  :: Natural
  -> ( Storable.Vector Vec3.Packed
     , Storable.Vector Word32
     )
generate_ level = (positions, indices)
  where
    (positions, _attrs, indices) =
      Icosphere.generateIndexed
        level
        mkInitialAttrs
        mkMidpointAttrs
        mkVertices

    mkInitialAttrs :: Vec3 -> ()
    mkInitialAttrs _pos = ()

    mkMidpointAttrs :: Float -> Vec3 -> () -> () -> ()
    mkMidpointAttrs _scale _midPos _attr1 _attr2 = ()

    mkVertices points _faces = do
      (rawPos, ()) <- points
      let normPos = Vec3.normalize rawPos
      pure (Vec3.Packed normPos, ())
