{-# LANGUAGE DeriveAnyClass #-}
{-# OPTIONS_GHC -Wwarn=unused-top-binds #-}

-- {-# OPTIONS_GHC -fforce-recomp #-}

module Stage.Main.Resource.CubeMap
  ( Collection(..)
  , Textures
  , sources
  , indices
  , numCubes
  ) where

import RIO

import GHC.Generics (Generic1)
import Global.Resource.CubeMap.Base qualified as Base

import Resource.Collection (Generically1(..), enumerate, size)
import Resource.Source (Source(..))
import Resource.Static as Static
import Resource.Texture (Texture, CubeMap)

data Collection a = Collection
  { base   :: Base.Collection a
  , clouds :: a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic1)
  deriving Applicative via (Generically1 Collection)

type Textures = Collection (Texture CubeMap)

Static.filePatterns Static.Files "resources/cubemaps"

sources :: Collection Source
sources = Collection
  { base   = Base.sources
  , clouds = File Nothing CLOUDS_KTX2
  }

indices :: Collection Int32
indices = fmap fst $ enumerate sources

numCubes :: Num a => a
numCubes = size sources
