{-# LANGUAGE DeriveAnyClass #-}

-- {-# OPTIONS_GHC -fforce-recomp #-}

module Stage.Main.Resource.Texture
  ( Collection(..)
  , Textures
  , sources
  , indices
  , numTextures
  ) where

import RIO

import GHC.Generics (Generic1)
import Global.Resource.Texture.Base qualified as Base
import Resource.Collection (Generically1(..), enumerate, size)
import Resource.Source (Source)
-- import Resource.Static as Static
import Resource.Texture (Texture, Flat)

data Collection a = Collection
  { base :: Base.Collection a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic1)
  deriving Applicative via (Generically1 Collection)

type Textures = Collection (Texture Flat)

-- Static.filePatterns Static.Files "resources/textures"

sources :: Collection Source
sources = Collection
  { base = Base.sources
  }

indices :: Collection Int32
indices = fmap fst $ enumerate sources

numTextures :: Num a => a
numTextures = size sources
