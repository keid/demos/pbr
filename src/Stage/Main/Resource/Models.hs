{-# LANGUAGE OverloadedLists #-}

module Stage.Main.Resource.Models
  ( Collection(..)
  , allocate

  , coloredSphere
  ) where

import RIO

import Engine.Types (StageRIO)
import Engine.Vulkan.Types (Queues)
import Geomancy (Transform, Vec3, vec2, vec3)
import Geomancy.Vec3 qualified as Vec3
import Geometry.Icosphere qualified as Icosphere
import Render.Lit.Colored.Model qualified as LitColored
import Resource.Buffer as Buffer
import Resource.Model qualified as Model
import Resource.Region qualified as Region
import RIO.Vector qualified as Vector
import RIO.Vector.Storable qualified as Storable
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Component.DynamicMaterial.Models qualified as DynamicMaterial

data Collection = Collection
  { zeroTransform   :: Buffer.Allocated 'Buffer.Staged Transform
  , coloredSpheres  :: Vector (LitColored.Model 'Buffer.Staged)
  , materialSpheres :: Vector DynamicMaterial.StagedModel
  }

allocate
  :: Queues Vk.CommandPool
  -> StageRIO env (Resource.ReleaseKey, Collection)
allocate pools = Region.run do
  zeroTransform <-
    Buffer.createStaged
      (Just "zeroTransform")
      pools
      Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
      1
      [mempty]
  Region.local_ $ Buffer.register zeroTransform

  metals <- for [0..num] \ix ->
    Model.createStaged
      (Just $ "Metal " <> fromString (show ix))
      pools
      (Storable.map (setPos (ix - num / 2) (-3)) positions)
      (Storable.map (setMR 1.0 $ ix / num) attrs)
      indices
  traverse_ Model.registerIndexed_ metals

  dielectrics <- for [0..num] \ix ->
    Model.createStaged
      (Just $ "Dielectric " <> fromString (show ix))
      pools
      (Storable.map (setPos (ix - num / 2) 3) positions)
      (Storable.map (setMR 0.0 $ ix / num) attrs)
      indices
  traverse_ Model.registerIndexed_ dielectrics

  materialSpheres <- Vector.generateM 3 \matIx ->
    DynamicMaterial.materialSphere pools (fromIntegral matIx)
  traverse_ Model.registerIndexed_ materialSpheres

  pure Collection
    { coloredSpheres =
        metals <> dielectrics
    , ..
    }
  where
    num = 10

    (positions, attrs, indices) = coloredSphere

    setPos offX offY (Vec3.Packed vertexPos) =
      Vec3.Packed $
        vertexPos + vec3 (offX * 3) offY 0

    setMR metallic roughness va = va
      { LitColored.vaBaseColor = 1
          -- Vec4.lerp
          --   roughness
          --   (vec4 1 0.5 metallic 1)
          --   (vec4 0.5 1 metallic 1)
      , LitColored.vaMetallicRoughness =
          vec2 metallic (max 0.05 roughness)
      }

coloredSphere
  :: ( Storable.Vector Vec3.Packed
     , Storable.Vector LitColored.VertexAttrs
     , Storable.Vector Word32
     )
coloredSphere =
  Icosphere.generateIndexed
    4
    mkInitialAttrs
    mkMidpointAttrs
    mkVertices
  where
    mkInitialAttrs :: Vec3 -> ()
    mkInitialAttrs _pos = ()

    mkMidpointAttrs :: Float -> Vec3 -> () -> () -> ()
    mkMidpointAttrs _scale _midPos _attr1 _attr2 = ()

    mkVertices points _faces = do
      (rawPos, ()) <- points
      let normPos = Vec3.normalize rawPos
      pure
        ( Vec3.Packed normPos
        , LitColored.VertexAttrs
            { vaBaseColor         = 1
            , vaEmissiveColor     = 0
            , vaMetallicRoughness = 0
            , vaNormal            = Vec3.Packed normPos
            }
        )
