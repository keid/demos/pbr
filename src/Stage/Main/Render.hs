module Stage.Main.Render
  ( updateBuffers
  , recordCommands
  ) where

import RIO.Local

import Engine.Types qualified as Engine
import Engine.Vulkan.DescSets (withBoundDescriptorSets0)
import Engine.Vulkan.Pipeline qualified as Pipeline
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Render.Draw qualified as Draw
import Render.ForwardMsaa qualified as ForwardMsaa
import Render.ImGui qualified as ImGui
import Render.ShadowMap.RenderPass qualified as ShadowPass
import Vulkan.Core10 qualified as Vk

import Component.DynamicMaterial.Observer qualified as DynamicMaterial
import Stage.Main.Render.Scene (prepareCasters, prepareScene)
import Stage.Main.Render.UI (imguiDrawData)
import Stage.Main.Types (FrameResources(..), RunState(..))

updateBuffers
  :: RunState
  -> FrameResources
  -> Basic.StageFrameRIO FrameResources RunState ()
updateBuffers RunState{..} FrameResources{..} = do
  Set0.observe rsSceneP frScene
  Set0.observe rsSceneUiP frSceneUi
  DynamicMaterial.observe (snd rsMaterials) frMaterials

recordCommands
  :: Vk.CommandBuffer
  -> FrameResources
  -> Word32
  -> Basic.StageFrameRIO FrameResources RunState ()
recordCommands cb fr@FrameResources{..} imageIndex = do
  (_context, Engine.Frame{fSwapchainResources, fRenderpass, fPipelines}) <- ask

  sceneDrawCasters <- prepareCasters fr
  (sceneDepthPrepass, sceneDrawOpaque, sceneDrawBlended) <- prepareScene fPipelines fr
  dear <- imguiDrawData

  updateShadow <- gets rsUpdateShadow
  Worker.observeIO_ updateShadow frUpdateShadow \() () -> do
    Worker.Versioned{vVersion} <- readTVarIO (Worker.getInput updateShadow)
    logDebug $ "Updating shadowmap for version " <> displayShow vVersion
    let shadowLayout = Pipeline.pLayout $ Basic.pShadowCast fPipelines
    ShadowPass.usePass (Basic.rpShadowPass fRenderpass) imageIndex cb do
      let shadowArea = ShadowPass.smRenderArea (Basic.rpShadowPass fRenderpass)
      Swapchain.setDynamic cb shadowArea shadowArea
      withBoundDescriptorSets0 cb Vk.PIPELINE_BIND_POINT_GRAPHICS shadowLayout frSunDescs $
        Graphics.bind cb (Basic.pShadowCast fPipelines) $
          sceneDrawCasters cb

  ForwardMsaa.usePass (Basic.rpForwardMsaa fRenderpass) imageIndex cb do
    Swapchain.setDynamicFullscreen cb fSwapchainResources

    Set0.withBoundSet0 frScene (Basic.pWireframe fPipelines) cb do
      Graphics.bind cb (Basic.pDepthOnly fPipelines) $
        sceneDepthPrepass cb

      sceneDrawOpaque cb

      Graphics.bind cb (Basic.pSkybox fPipelines) $
        Draw.triangle_ cb

      sceneDrawBlended cb

    ImGui.draw dear cb
