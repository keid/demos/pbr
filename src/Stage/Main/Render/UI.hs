module Stage.Main.Render.UI
  ( imguiDrawData
  ) where

import RIO.Local

import DearImGui qualified
import Engine.Types (StageFrameRIO)
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.ImGui qualified as ImGui
import RIO.List qualified as List

import Stage.Main.Types (FrameResources(..), RunState(..))
import Stage.Main.UI qualified as UI
import Component.DynamicMaterial.UI qualified as Pawn

type DrawM = StageFrameRIO Basic.RenderPasses Basic.Pipelines FrameResources RunState

imguiDrawData :: DrawM DearImGui.DrawData
imguiDrawData = do
  ui <- gets rsUI
  visible <- Worker.getOutputData $ UI.visible ui

  fmap snd . ImGui.mkDrawData $
    when visible do
      DearImGui.withWindowOpen "Materials" do
        sequence_ $ List.intersperse DearImGui.separator do
          (ix, material) <- zip [1..] $ toList (UI.materials ui)
          pure $ DearImGui.withID ix do
            DearImGui.text $ "Material " <> fromString (show @Int ix)
            Pawn.ui material
