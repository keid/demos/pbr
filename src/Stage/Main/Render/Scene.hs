{-# LANGUAGE OverloadedLists #-}

{-# OPTIONS_GHC -fno-warn-redundant-constraints #-}

module Stage.Main.Render.Scene
  ( prepareCasters
  , prepareScene
  ) where

import RIO.Local

import Engine.Types (StageFrameRIO)
import Engine.Vulkan.DescSets (Bound, Compatible)
import Engine.Vulkan.Pipeline.Graphics qualified as Graphics
import Geomancy (Transform)
import Render.Basic qualified as Basic
import Render.DescSets.Set0 (Scene)
import Render.DescSets.Sun (Sun)
import Render.Draw qualified as Draw
import Resource.Model (IndexRange(..))
import Resource.Model qualified as Model
import Resource.Buffer qualified as Buffer
import RIO.Vector qualified as Vector
import Vulkan.Core10 qualified as Vk

import Stage.Main.Resource.Models qualified as Models
import Stage.Main.Types (FrameResources(..), RunState(..))

type DrawM = StageFrameRIO Basic.RenderPasses Basic.Pipelines FrameResources RunState

type DrawCasters dsl m = Vk.CommandBuffer -> Bound dsl (Model.Vertex3d ()) Transform m ()

type DrawData dsl m =
  ( Vk.CommandBuffer -> Bound dsl (Model.Vertex3d ()) Transform m () -- depth pre-pass
  , Vk.CommandBuffer -> Bound dsl Void Void m () -- opaque
  , Vk.CommandBuffer -> Bound dsl Void Void m () -- blended
  )

prepareCasters
  :: ( Compatible '[Sun] dsl
     )
  => FrameResources
  -> DrawM (DrawCasters dsl DrawM)
prepareCasters FrameResources{..} = do
  models <- gets rsModels
  pure \cb -> do
    for_ (Models.coloredSpheres models) \model -> do
      Draw.indexedPos cb model (Models.zeroTransform models)

    let
      parts = do
        model <- toList $ Models.materialSpheres models
        pure $ IndexRange 0 (Buffer.aUsed $ Model.iIndices model)
    flip Vector.imapM_ (Models.materialSpheres models) \ix model ->
      Draw.unsafeIndexedParts False cb model frTransforms ix (take (ix + 1) parts)

prepareScene
  :: ( Compatible '[Scene] dsl
     )
  => Basic.Pipelines
  -> FrameResources
  -> DrawM (DrawData dsl DrawM)
prepareScene Basic.Pipelines{..} FrameResources{..} = do
  models <- gets rsModels

  let
    drawDepth cb = do
      -- XXX: opaque stuff for material pipeline
      let
        parts = do
          model <- toList $ Models.materialSpheres models
          pure $ IndexRange 0 (Buffer.aUsed $ Model.iIndices model)
      flip Vector.imapM_ (Models.materialSpheres models) \ix model ->
        Draw.unsafeIndexedParts False cb model frTransforms ix (take (ix + 1) parts)

    drawOpaque cb = do
      Graphics.bind cb pLitColored do
        for_ (Models.coloredSpheres models) \model ->
          Draw.indexed cb model (Models.zeroTransform models)

      Graphics.bind cb pLitMaterial do
        let
          parts = do
            model <- toList $ Models.materialSpheres models
            pure $ IndexRange 0 (Buffer.aUsed $ Model.iIndices model)
        flip Vector.imapM_ (Models.materialSpheres models) \ix model -> do
          Draw.indexedParts True cb model frTransforms ix (take (ix + 1) parts)

    drawBlended _cb = do
      pure ()

  pure (drawDepth, drawOpaque, drawBlended)
