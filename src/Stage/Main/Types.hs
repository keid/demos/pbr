module Stage.Main.Types
  ( Stage
  , Frame

  , FrameResources(..)
  , RunState(..)
  ) where

import RIO

import Data.Tagged (Tagged)
import Engine.Camera qualified as Camera
import Engine.Camera.Controls qualified as CameraControls
import Engine.Events qualified as Events
import Engine.Worker (ObserverIO)
import Engine.Worker qualified as Worker
import Geomancy (Transform, Vec2)
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Render.DescSets.Sun (Sun)
import Resource.Buffer qualified as Buffer
import Vulkan.Core10 qualified as Vk

import Component.DynamicMaterial.Observer qualified as DynamicMaterials
import Component.DynamicMaterial.Process qualified as DynamicMaterials
import Stage.Main.Event.Types (Event)
import Stage.Main.Resource.CubeMap qualified as CubeMap
import Stage.Main.Resource.Models qualified as Models
import Stage.Main.Resource.Texture qualified as Texture
import Stage.Main.UI (UI)
import Stage.Main.World.Scene qualified as Scene

type Stage = Basic.Stage FrameResources RunState

type Frame = Basic.Frame FrameResources

data FrameResources = FrameResources
  { frScene   :: Set0.FrameResource '[Set0.Scene]
  , frSceneUi :: Set0.FrameResource '[Set0.Scene]

  , frSunDescs :: Tagged '[Sun] (Vector Vk.DescriptorSet)
  , frSunData  :: Buffer.Allocated 'Buffer.Coherent Sun
  , frUpdateShadow :: ObserverIO ()

  , frMaterials :: DynamicMaterials.Observer
  , frTransforms :: Buffer.Allocated 'Buffer.Coherent Transform
  }

data RunState = RunState
  { rsEvents      :: Maybe (Events.Sink Event RunState)

  , rsViewP          :: Camera.ViewProcess
  , rsCameraControls :: CameraControls.ControlsProcess

  , rsCursorPos :: Worker.Var Vec2
  , rsCursorP   :: Worker.Merge Vec2

  , rsSceneP :: Scene.Process
  , rsSceneV :: Scene.InputVar

  , rsSceneUiP :: Scene.Process

  , rsUI :: UI

  , rsUpdateShadow :: Worker.Var ()

  , rsModels :: Models.Collection
  , rsTextures :: Texture.Textures
  , rsCubeMaps :: CubeMap.Textures

  , rsMaterials :: (Vector DynamicMaterials.Cell, DynamicMaterials.Merge)
  }
