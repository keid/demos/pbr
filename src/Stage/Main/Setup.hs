{-# LANGUAGE OverloadedLists #-}

module Stage.Main.Setup
  ( stackStage
  , Options(..)
  ) where

import RIO.Local

import Control.Monad.Trans.Resource (ResourceT)
import Engine.Camera qualified as Camera
import Engine.Camera.Controls qualified as CameraControls
import Engine.Types (StackStage(..), StageRIO)
import Engine.Types qualified as Engine
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Vulkan.Types (Queues)
import Engine.Worker qualified as Worker
import Geomancy (vec2, pattern WithVec2)
import Geomancy.Transform qualified as Transform
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Render.DescSets.Sun qualified as Sun
import Render.ImGui qualified as ImGui
import Render.Samplers qualified as Samplers
import Render.ShadowMap.RenderPass qualified as ShadowPass
import Resource.Buffer as Buffer
import Resource.CommandBuffer (withPools)
import Resource.Image qualified as Image
import Resource.Region qualified as Region
import Resource.Texture.Ktx2 qualified as Ktx2
import RIO.State (modify')
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Component.DynamicMaterial.Observer qualified as DynamicMaterial
import Component.DynamicMaterial.Process qualified as DynamicMaterial
import Stage.Main.Events qualified as Events
import Stage.Main.Render qualified as Render
import Stage.Main.Resource.CubeMap qualified as CubeMap
import Stage.Main.Resource.Models qualified as Models
import Stage.Main.Resource.Texture qualified as Texture
import Stage.Main.Types (FrameResources(..), RunState(..), Stage)
import Stage.Main.UI qualified as UI
import Stage.Main.World.Scene qualified as Scene

data Options = Options
  deriving (Show, Generic)

stackStage :: StackStage
stackStage = StackStage stage

stage :: Stage
stage = Engine.Stage
  { sTitle = "Main"

  , sAllocateRP = Basic.allocate_
  , sAllocateP  = allocatePipelines
  , sInitialRS  = initialRunState
  , sInitialRR  = initialFrameResources
  , sBeforeLoop = beforeLoop

  , sUpdateBuffers  = Render.updateBuffers
  , sRecordCommands = Render.recordCommands

  , sAfterLoop = afterLoop
  }
  where
    allocatePipelines swapchain rps = do
      void $! ImGui.allocate swapchain (Basic.rpForwardMsaa rps) 0

      samplers <- Samplers.allocate (Swapchain.getAnisotropy swapchain)
      let sceneBinds = Set0.mkBindings samplers Texture.sources CubeMap.sources 1
      Basic.allocatePipelines sceneBinds (Swapchain.getMultisample swapchain) rps

    beforeLoop = do
      (key, sink) <- Events.spawn
      modify' \ rs -> rs
        { rsEvents = Just sink
        }

      ImGui.beforeLoop True

      pure key

    afterLoop key = do
      ImGui.afterLoop
      Resource.release key

initialRunState :: StageRIO env (Resource.ReleaseKey, RunState)
initialRunState = withPools \pools -> Region.run do
  rsTextures <- traverse (Ktx2.load pools) Texture.sources
  rsCubeMaps <- traverse (Ktx2.load pools) CubeMap.sources

  perspective <- Camera.spawnPerspective
  ortho <- Camera.spawnOrthoPixelsCentered

  screen <- Engine.askScreenVar

  rsCursorPos <- Worker.newVar 0
  rsCursorP <-
    Worker.spawnMerge2
      (\Vk.Extent2D{width, height} (WithVec2 windowX windowY) ->
          vec2
            (windowX - fromIntegral width / 2)
            (windowY - fromIntegral height / 2)
      )
      screen
      rsCursorPos

  rsViewP <- CameraControls.spawnViewOrbital Camera.initialOrbitalInput

  rsCameraControls <- CameraControls.spawnControls rsViewP

  rsSceneV <- Worker.newVar Scene.initialInput
  rsSceneP <- Worker.spawnMerge3 Scene.mkScene perspective rsViewP rsSceneV

  rsSceneUiP <- Worker.spawnMerge1 Scene.mkSceneUi ortho

  rsUpdateShadow <- Worker.newVar ()

  let
    materials =
      [ DynamicMaterial.Input
          { metal = False
          , roughness = 0.5
          , baseColor = 1
          }
      , DynamicMaterial.Input
          { metal = True
          , roughness = 0.75
          , baseColor = 1
          }
      , DynamicMaterial.Input
          { metal = True
          , roughness = 0.25
          , baseColor = 1
          }
      ]
  rsMaterials <- DynamicMaterial.spawn materials

  rsModels <- Region.local $
    Models.allocate pools

  -- (screenKey, rsScreenBoxP) <- Worker.registered Layout.trackScreen
  rsUI <- Region.local $
    UI.allocate $ fst rsMaterials

  let rsEvents = Nothing
  pure RunState{..}

initialFrameResources
  :: Queues Vk.CommandPool
  -> Basic.RenderPasses
  -> Basic.Pipelines
  -> ResourceT (Engine.StageRIO RunState) FrameResources
initialFrameResources _pools passes pipelines = do
  (_materialCells, materialsMerged) <- gets rsMaterials
  frMaterials <- DynamicMaterial.newObserver 64 -- $ length materialCells
  lift $ DynamicMaterial.observe materialsMerged frMaterials
  materialsBuffer <- Worker.readObservedIO frMaterials
  -- traceShowM materialsBuffer

  let (_sunBB, lights) = Scene.staticLights

  frTransforms <- Region.local $ Buffer.allocateCoherent
    (Just "frTransforms")
    Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
    0
    [ Transform.translate (-2) 0 0
    , Transform.translate 2 0 0
    , Transform.translate 0 (-1) 0
    ]

  lightsData <- Region.local $ Buffer.allocateCoherent
    (Just "lightsData")
    Vk.BUFFER_USAGE_UNIFORM_BUFFER_BIT
    0
    lights

  textures <- gets rsTextures
  cubemaps <- gets rsCubeMaps

  frScene <- Set0.allocate
    (Basic.getSceneLayout pipelines)
    textures
    cubemaps
    (Just lightsData)
    [ Image.aiImageView . ShadowPass.smDepthImage $
        Basic.rpShadowPass passes
    ]
    (Just materialsBuffer)

  frSceneUi <- Set0.allocateEmpty
    (Basic.getSceneLayout pipelines)

  (frSunDescs, frSunData) <- Sun.createSet0Ds (Basic.getSunLayout pipelines)
  Buffer.updateCoherent lights frSunData

  frUpdateShadow <- Worker.newObserverIO ()

  pure FrameResources{..}
