module Stage.Main.UI
  ( UI(..)
  , allocate
  ) where

import RIO

import Control.Monad.Trans.Resource qualified as Resource
import Engine.Types qualified as Engine
import Engine.Worker qualified as Worker

import Component.DynamicMaterial.Process qualified as DynamicMaterial

data UI = UI
  { visible   :: Worker.Var Bool
  , materials :: Vector DynamicMaterial.Cell
  }

allocate
  :: Vector DynamicMaterial.Cell
  -> Engine.StageRIO env (Resource.ReleaseKey, UI)
allocate materials = do
  visible <- Worker.newVar True

  key <- Resource.register $ traverse_ Resource.release
    []

  pure (key, UI{..})
