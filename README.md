# Physically based rendering

Simple scene with environment map, image-based and, optionally, directional lighting.

Editable material is presented as a component, with models, processes and UI modules.

![2021-08-29](https://i.imgur.com/RtNhptE.png)

* https://gitlab.com/keid/engine/-/tree/main/render-basic
* https://gitlab.com/keid/engine/-/tree/main/ui-dearimgui

## Assets

* Cloud skybox: https://opengameart.org/content/sky-box-sunny-day
